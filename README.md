# Soothing 32

Lightweight yet aesthetically pleasing texture pack for Minetest. Seriously, it could run on a toaster! :bread:  
The texture pack is 32 colours only, more exactly the ones in the [Zughy32 palette](https://lospec.com/palette-list/zughy-32).

<a href="https://liberapay.com/EticaDigitale/donate"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

![Soothing 32](screenshot.png)  

### Supported mods
* Bucket
* Carts
* Default
* Doors
* Flowers
* Wool
